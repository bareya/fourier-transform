# Discrete Fourier Transform 

Bunch of observations about discrete fourier transform.

## Basic function low frequency pass filter 

Trivial function

```
f(x) = 2 + 2sin(0.5x) + 0.5sin(2x) + cos(4x)
```

|                           Approximation |                           Error |                           Density |
|                                     --- |                             --- |                               --- |
| ![](images/dft_simple_approximation-0.250.svg) | ![](images/dft_simple_error-0.250.svg) | ![](images/dft_simple_density-0.250.svg) |
| ![](images/dft_simple_approximation-0.500.svg) | ![](images/dft_simple_error-0.500.svg) | ![](images/dft_simple_density-0.500.svg) |
| ![](images/dft_simple_approximation-0.750.svg) | ![](images/dft_simple_error-0.750.svg) | ![](images/dft_simple_density-0.750.svg) |
| ![](images/dft_simple_approximation-1.000.svg) | ![](images/dft_simple_error-1.000.svg) | ![](images/dft_simple_density-1.000.svg) |

## Moon's elevation


|                           Approximation |                           Error |                           Density |
|                                     --- |                             --- |                               --- |
| ![](images/dft_moons_approximation-0.010.svg) | ![](images/dft_moons_error-0.010.svg) | ![](images/dft_moons_density-0.010.svg) |
| ![](images/dft_moons_approximation-0.025.svg) | ![](images/dft_moons_error-0.025.svg) | ![](images/dft_moons_density-0.025.svg) |
| ![](images/dft_moons_approximation-0.050.svg) | ![](images/dft_moons_error-0.050.svg) | ![](images/dft_moons_density-0.050.svg) |
| ![](images/dft_moons_approximation-0.100.svg) | ![](images/dft_moons_error-0.100.svg) | ![](images/dft_moons_density-0.100.svg) |
| ![](images/dft_moons_approximation-1.000.svg) | ![](images/dft_moons_error-1.000.svg) | ![](images/dft_moons_density-1.000.svg) |
