/*
MIT License

Copyright (c) 2021 Piotr Barejko

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#ifndef FOURIER_TRANSFORM_TIFF_HPP
#define FOURIER_TRANSFORM_TIFF_HPP

#include <tiffio.hxx>

#include <cassert>
#include <cstdint>
#include <vector>

///
/// \brief Manages lifetime of the tiff object
///
class TiffReader {
public:
    explicit TiffReader(const char *name) {
        _tif = TIFFOpen(name, "r");
        if (!_tif) {
            using namespace std::string_literals;
            throw std::runtime_error{"can not open file: "s + name};
        }
    }

    uint32 get_field(uint32 tag) const {
        uint32 value;
        TIFFGetField(_tif, tag, &value);
        return value;
    }

    uint32 width() const {
        return get_field(TIFFTAG_IMAGEWIDTH);
    }

    uint32 height() const {
        return get_field(TIFFTAG_IMAGELENGTH);
    }

    uint32 num_pixels() const {
        return width() * height();
    }

    uint32 bits_per_sample() const {
        return get_field(TIFFTAG_BITSPERSAMPLE);
    }

    uint32 samples_per_pixel() const {
        return get_field(TIFFTAG_SAMPLESPERPIXEL);
    }

    template<typename T>
    std::vector<T> create_line_data() const noexcept {
        std::vector<T> data;
        data.resize(width(), T{});
        return data;
    }

    template<typename T>
    void read_data(std::vector<T>& data, uint32 y) const noexcept {
        assert(data.size() == width());
        TIFFReadScanline(_tif, &data[0], y);
    }

    template<typename T>
    void read_data(T* data, uint32 y) const noexcept {
        TIFFReadScanline(_tif, data, y);
    }

    ~TiffReader() {
        TIFFClose(_tif);
    }

private:
    TIFF *_tif;
};

#endif//FOURIER_TRANSFORM_TIFF_HPP
