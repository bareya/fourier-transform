/*
MIT License

Copyright (c) 2021 Piotr Barejko

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef FOURIER_TRANSFORM_FOURIER_HPP
#define FOURIER_TRANSFORM_FOURIER_HPP

#include "complex.hpp"
#include "polar.hpp"

#include <algorithm>
#include <cassert>

enum class FourierTransform {
    Forward = 1,
    Reverse = -1
};

inline void dft(std::vector<Complex> &weights, const FourierTransform &type = FourierTransform::Forward) {
    const size_t N = weights.size();
    const float sample_spacing = static_cast<float>(type) * 2.0f * M_PI / N;

    std::vector<Complex> transform;
    transform.resize(weights.size());

    for (size_t k{}; k < N; ++k) {
        const float sample_time = sample_spacing * k;

        for (size_t n{}; n < N; ++n) {
            const float angle = sample_time * n;
            transform[k] = transform[k] + weights[n] * Complex{std::cos(angle), -std::sin(angle)};
        }

        if (type == FourierTransform::Reverse) transform[k] = transform[k] * static_cast<float>(1.0f / N);
    }

    std::swap(weights, transform);
}

template<typename T>
std::vector<T> slice(typename std::vector<T>::const_iterator begin,//
                     typename std::vector<T>::const_iterator end,  //
                     size_t stride) {
    stride = std::max(static_cast<size_t>(1), stride);

    const auto dist = std::distance(begin, end);
    const size_t new_size = std::ceil(dist / static_cast<float>(stride));
    std::vector<T> result;
    result.reserve(new_size);

    for (; begin < end; begin += stride) {
        result.push_back(*begin);
    }
    return result;
}

///
/// 1D Fast Fourier Transform - must be power of two, radix-2 Cooley–Tukey implementation
///
inline void fft(std::vector<Complex> &weights, const FourierTransform &type = FourierTransform::Forward) {
    const size_t N = weights.size();
    if (N == 1) {
        return;
    }

    // group by even and odd
    std::vector<Complex> e = slice<Complex>(weights.begin(), weights.end(), 2);
    std::vector<Complex> o = slice<Complex>(weights.begin() + 1, weights.end(), 2);

    fft(e, type);
    fft(o, type);

    const float sample_spacing = static_cast<float>(type) * 2.0f * M_PI / N;
    const size_t hN = N / 2;

    Complex weight{1};
    const Complex weight_exp{std::cos(sample_spacing), -std::sin(sample_spacing)};

    for (size_t i{}; i < hN; ++i) {
        weights[i] = e[i] + weight * o[i];
        weights[hN + i] = e[i] - weight * o[i];
        weight *= weight_exp;

        if (type == FourierTransform::Reverse) {
            weights[i] *= 0.5;
            weights[hN + i] *= 0.5;
        }
    }
}

inline void fourier_transform(std::vector<Complex> &weights, const FourierTransform &type) {
    if (weights.size() % 2 == 0) {
        fft(weights, type);
    } else {
        std::vector<Complex> even_weights;
        even_weights.reserve(weights.size());
        std::copy(weights.begin(), weights.end(), std::back_inserter(even_weights));
        even_weights.push_back(even_weights.back());

        fft(even_weights, type);

        for (size_t i{}; i < even_weights.size() - 1; ++i) {
            weights[i] = even_weights[i];
        }
    }
}

inline std::vector<Polar> spectral_density(const std::vector<Complex> &trans) {
    const size_t N = trans.size();

    std::vector<Polar> density;
    density.reserve(N);

    // DC amplitude approximation, 1.0 because we use all samples
    if (!trans.empty()) {
        float dc_ampl_avg = 1.0f / N;
        density.emplace_back(trans[0]);
        density.back().ampl() *= dc_ampl_avg;
    }

    // AC amplitude average, 2.0 because of the AC symmetry
    // in cos approximation we use only first half
    float ac_ampl_avg = 2.0f / N;
    for (size_t i{1}; i < N; ++i) {
        density.emplace_back(trans[i]);
        density.back().ampl() *= ac_ampl_avg;
    }

    return density;
}

inline float cos_approximation(const std::vector<Polar> &density, float x, float threshold) noexcept {
    if (density.empty()) return 0;

    const size_t N = density.size();

    float approx{};
    const float period_inc = static_cast<float>(2.0f * M_PI) / N;

    // DC component and half of AC since AC is symmetric times threshold
    const size_t freq_percent = N / 2 * threshold;
    for (size_t k{}; k < freq_percent; ++k) {

        approx += density[k].ampl() * std::cos(k * x * period_inc + density[k].phas());
    }

    return approx;
}

inline std::vector<float> cos_approximation(const std::vector<Polar> &density, float threshold) {
    std::vector<float> approx;
    approx.reserve(density.size());

    for (size_t i{}; i < density.size(); ++i) {
        approx.emplace_back(cos_approximation(density, i, threshold));
    }

    return approx;
}

#endif//FOURIER_TRANSFORM_FOURIER_HPP
