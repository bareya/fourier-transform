/*
MIT License

Copyright (c) 2021 Piotr Barejko

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef FOURIER_TRANSFORM_COMPLEX_HPP
#define FOURIER_TRANSFORM_COMPLEX_HPP

struct Complex {
    float _re{};
    float _im{};

    explicit Complex(float r = 0.0, float i = 0.0) noexcept
        : _re{r}, _im{i} {}

    const float &real() const noexcept {
        return _re;
    }

    float &real() noexcept {
        return _re;
    }

    const float &imag() const noexcept {
        return _im;
    }

    float &imag() noexcept {
        return _im;
    }
};

inline Complex operator-(const Complex &lhs, const Complex &rhs) noexcept {
    return Complex{lhs.real() - rhs.real(), lhs.imag() - rhs.imag()};
}

inline Complex operator+(const Complex &lhs, const Complex &rhs) noexcept {
    return Complex{lhs.real() + rhs.real(), lhs.imag() + rhs.imag()};
}

inline Complex& operator+=(Complex &lhs, float rhs) noexcept {
    lhs.real() += rhs;
    lhs.imag() += rhs;
    return lhs;
}

inline Complex operator*(const Complex &lhs, float rhs) noexcept {
    return Complex{lhs.real() * rhs, lhs.imag() * rhs};
}

inline Complex& operator*=(Complex& lhs, float rhs) noexcept {
    lhs.real() *= rhs;
    lhs.imag() *= rhs;
    return lhs;
}

inline Complex operator*(const Complex &lhs, const Complex &rhs) noexcept {
    return Complex{lhs.real() * rhs.real() - lhs.imag() * rhs.imag(),
                   lhs.real() * rhs.imag() + lhs.imag() * rhs.real()};
}

inline Complex& operator*=(Complex& lhs, const Complex &rhs) noexcept {
    lhs = lhs * rhs;
    return lhs;
}

#endif//FOURIER_TRANSFORM_COMPLEX_HPP
