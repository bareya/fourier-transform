#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest/doctest.h>

#include "../source/complex.hpp"
#include "../source/fourier.hpp"
#include "../source/polar.hpp"
#include "../source/tiff.hpp"

#include <complex>
#include <gnuplot-iostream.h>
#include <iomanip>
#include <memory>
#include <sstream>
#include <vector>

namespace {

    template<typename T>
    void complex_number_tests() {
        T a{2, 3};
        a = a * 3.0f;

        REQUIRE(a.real() == doctest::Approx{6});
        REQUIRE(a.imag() == doctest::Approx{9});

        a = a + T{1, 2};
        REQUIRE(a.real() == doctest::Approx{7});
        REQUIRE(a.imag() == doctest::Approx{11});

        a = a * T{-2, 4};
        REQUIRE(a.real() == doctest::Approx{-58});
        REQUIRE(a.imag() == doctest::Approx{6});
    }

}// namespace


TEST_SUITE("testing complex number") {
    TEST_CASE("compare standard complex to homemade complex") {
        complex_number_tests<std::complex<float>>();
        complex_number_tests<Complex>();
    }
}

TEST_SUITE("testing polar coordinate") {
}

TEST_SUITE("testing fourier transform") {
    TEST_CASE("discrete fourier transform from wiki") {
        std::vector<Complex> input{
                Complex{1, 0},
                Complex{2, -1},
                Complex{0, -1},
                Complex{-1, 2},
        };

        SUBCASE("forward") {
            dft(input, FourierTransform::Forward);

            REQUIRE(input[0].real() == doctest::Approx{2});
            REQUIRE(input[0].imag() == doctest::Approx{0});

            REQUIRE(input[1].real() == doctest::Approx{-2});
            REQUIRE(input[1].imag() == doctest::Approx{-2});

            REQUIRE(input[2].real() == doctest::Approx{0});
            REQUIRE(input[2].imag() == doctest::Approx{-2});

            REQUIRE(input[3].real() == doctest::Approx{4});
            REQUIRE(input[3].imag() == doctest::Approx{4});
        }

        SUBCASE("reverse") {
            dft(input, FourierTransform::Forward);
            dft(input, FourierTransform::Reverse);

            REQUIRE(input[0].real() == doctest::Approx{1});
            REQUIRE(input[0].imag() == doctest::Approx{0});

            REQUIRE(input[1].real() == doctest::Approx{2});
            REQUIRE(input[1].imag() == doctest::Approx{-1});

            REQUIRE(input[2].real() == doctest::Approx{0});
            REQUIRE(input[2].imag() == doctest::Approx{-1});

            REQUIRE(input[3].real() == doctest::Approx{-1});
            REQUIRE(input[3].imag() == doctest::Approx{2});
        }
    }

    TEST_CASE("discrete fast fourier transform from wiki") {
        std::vector<Complex> input{
                Complex{1, 0},
                Complex{2, -1},
                Complex{0, -1},
                Complex{-1, 2},
        };

        SUBCASE("forward") {
            fft(input, FourierTransform::Forward);

            REQUIRE(input[0].real() == doctest::Approx{2});
            REQUIRE(input[0].imag() == doctest::Approx{0});

            REQUIRE(input[1].real() == doctest::Approx{-2});
            REQUIRE(input[1].imag() == doctest::Approx{-2});

            REQUIRE(input[2].real() == doctest::Approx{0});
            REQUIRE(input[2].imag() == doctest::Approx{-2});

            REQUIRE(input[3].real() == doctest::Approx{4});
            REQUIRE(input[3].imag() == doctest::Approx{4});
        }

        SUBCASE("reverse") {
            dft(input, FourierTransform::Forward);
            dft(input, FourierTransform::Reverse);

            REQUIRE(input[0].real() == doctest::Approx{1});
            REQUIRE(input[0].imag() == doctest::Approx{0});

            REQUIRE(input[1].real() == doctest::Approx{2});
            REQUIRE(input[1].imag() == doctest::Approx{-1});

            REQUIRE(input[2].real() == doctest::Approx{0});
            REQUIRE(input[2].imag() == doctest::Approx{-1});

            REQUIRE(input[3].real() == doctest::Approx{-1});
            REQUIRE(input[3].imag() == doctest::Approx{2});
        }
    }

    std::string float_to_string(float v, size_t p) {
        std::stringstream ss;
        ss << std::fixed << std::setprecision(p) << v;
        return ss.str();
    }

    void draw_graphs(const std::vector<Complex> &input, const float cutoff_threshold, const std::string &title,
                     bool draw_points = true,
                     bool smooth_curves = false) {
        const std::string cutoff_threshold_str = float_to_string(cutoff_threshold, 3);

        // compute dft and convert complex numbers into spectral density
        std::vector<Complex> dft_transform = input;
        fourier_transform(dft_transform, FourierTransform::Forward);
        const std::vector<Polar> density = spectral_density(dft_transform);

        // plot density
        {
            std::vector<std::pair<float, float>> xy_density;
            xy_density.reserve(density.size());
            for (size_t i{}; i < density.size(); ++i) {
                xy_density.emplace_back(i, density[i].ampl());
            }

            float percent = density.size() / 2 * cutoff_threshold;

            Gnuplot gp;
            gp << "set term svg size 1000 400\n";
            gp << "set output '" << PROJECT_IMAGES << "/dft_" << title << "_density-" << cutoff_threshold_str << ".svg'\n";
            gp << "set key on\n";
            gp << "set grid\n";
            gp << "set title 'Spectral Density(threshold=" << cutoff_threshold_str << ")'\n";
            gp << "set xlabel 'k'\n";
            gp << "set ylabel 'apmlitude'\n";
            gp << "set samples 100\n";
            gp << "set arrow from " << percent << ", graph 0 to " << percent << ", graph 1 nohead lc rgb 'dark-violet' dashtype 3\n";
            gp << "plot ";
            gp << gp.file1d(xy_density) << " with impulse linetype rgb 'black' linewidth 4 title 'apmlitude'";//
            gp << "\n";
        }

        // using spectral density use cosine approximation to rebuild input signal
        const std::vector<float> approximation = cos_approximation(density, cutoff_threshold);

        // plot input data and cos approximation
        {
            std::vector<std::pair<float, float>> xy_input;
            xy_input.reserve(input.size());
            for (size_t i{}; i < input.size(); ++i) {
                xy_input.emplace_back(i, input[i].real());
            }

            std::vector<std::pair<float, float>> xy_approx;
            xy_approx.reserve(approximation.size());
            for (size_t i{}; i < approximation.size(); ++i) {
                xy_approx.emplace_back(i, approximation[i]);
            }

            Gnuplot gp;
            gp << "set term svg size 1000 400\n";
            gp << "set output '" << PROJECT_IMAGES << "/dft_" << title << "_approximation-" << cutoff_threshold_str << ".svg'\n";
            gp << "set key on\n";
            gp << "set title 'Cosine Approximation(threshold=" << cutoff_threshold_str << ")'\n";
            gp << "set grid\n";
            gp << "set xlabel 'k'\n";
            gp << "set ylabel 'f(k)'\n";
            gp << "set samples 200\n";
            gp << "set xzeroaxis linestyle 1  linetype rgb \"black\" linewidth 1\n";
            gp << "plot ";
            if (draw_points) {
                gp << gp.file1d(xy_input) << "with points pointtype 7 pointsize 0.35 linetype rgb 'black' notitle,";
                gp << gp.file1d(xy_approx) << "with points pointtype 2 pointsize 1 linewidth 2 linetype rgb 'dark-violet' notitle,";
            }
            gp << gp.file1d(xy_input) << "with lines " << (smooth_curves ? "smooth  csplines" : "") << " linewidth 0.5 linetype rgb 'black'  title 'input',";
            gp << gp.file1d(xy_approx) << "with lines " << (smooth_curves ? "smooth csplines" : "") << " linewidth 0.5 linetype rgb 'dark-violet' title 'approximation'";
            gp << '\n';
        }

        // plot relative error
        {
            std::vector<std::pair<float, float>> xy_error;
            xy_error.reserve(approximation.size());
            for (size_t i{}; i < approximation.size(); ++i) {
                float error = std::abs(input[i].real() - approximation[i]);
                xy_error.emplace_back(i, error);
            }

            Gnuplot gp;
            gp << "set term svg size 1000 400\n";
            gp << "set output '" << PROJECT_IMAGES << "/dft_" << title << "_error-" << cutoff_threshold_str << ".svg'\n";
            gp << "set key on\n";
            gp << "set title 'Error(threshold=" << cutoff_threshold_str << ")'\n";
            gp << "set grid\n";
            gp << "set xlabel 'k'\n";
            gp << "set ylabel 'abs(f(k) - a(k))'\n";
            gp << "set samples 200\n";
            gp << "set xzeroaxis linestyle 1  linetype rgb \"black\" linewidth 1\n";
            gp << "plot ";
            if (draw_points) {
                gp << gp.file1d(xy_error) << "with points pointtype 7 pointsize 0.35 linetype rgb 'black' notitle,";
            }
            gp << gp.file1d(xy_error) << "with lines linewidth 0.5 linetype rgb 'black'  title 'error',";
            gp << '\n';
        }
    }

    TEST_CASE("discrete fourier transform cos approximation") {
        auto approx_func = [](float x) -> float {
            return 2.0f + 2 * std::sin(x * 0.5f) + 0.5f * std::sin(x * 2.0f) + std::cos(x * 4.0f);
        };

        const size_t num_samples = 80;
        auto create_input = [&num_samples, &approx_func]() {
            std::vector<Complex> input;
            for (size_t i{}; i < num_samples; ++i) {
                float x_inc = (2.0f * M_PI) / (num_samples);
                float x = i * x_inc;
                input.emplace_back(approx_func(x), 0.0f);
            }
            return input;
        };

        // create some input data
        const std::vector<Complex> input = create_input();

        for (auto cutoff_threshold : {0.25f, 0.5f, 0.75f, 1.0f}) {
            draw_graphs(input, cutoff_threshold, "simple", true, true);
        }
    }

    struct BinHeader {
        unsigned int element_size;
        unsigned int num_elements;
    };

    struct BinReader {
        explicit BinReader(const std::string &bin_file_name) {
            std::ifstream ifs{bin_file_name};
            if (!ifs.is_open()) throw std::runtime_error{"can't open bin file!"};

            BinHeader bin_header{};
            ifs.read(reinterpret_cast<char *>(&bin_header), sizeof(BinHeader));

            read_line = std::vector<float>(bin_header.num_elements, 0);
            ifs.read(reinterpret_cast<char *>(read_line.data()), bin_header.num_elements * bin_header.num_elements);
        }

        const std::vector<float> &data() const noexcept {
            return read_line;
        }

    private:
        std::vector<float> read_line;
    };

    std::string get_binary_file_name() {
        std::string bin_file_name{TIFF_FILE};
        auto pos = bin_file_name.rfind('.');
        return bin_file_name.substr(0, pos) + ".bin";
    }

    TEST_CASE("read moon's elevation from a file and dump it to a bin file") {
        // dump to bin file
        std::string bin_file_name = get_binary_file_name();

        std::vector<float> write_line;
        std::vector<float> read_line;

        // save data to a bin file
        {
            std::unique_ptr<TiffReader> tiff_reader;
            REQUIRE_NOTHROW(tiff_reader = std::make_unique<TiffReader>(TIFF_FILE));
            auto h = tiff_reader->height();
            auto w = tiff_reader->width();

            REQUIRE(h != 0);
            REQUIRE(w != 0);

            write_line = tiff_reader->create_line_data<float>();
            tiff_reader->read_data(write_line, h / 2);

            std::ofstream ofs(bin_file_name);
            BinHeader bin_header{sizeof(float), static_cast<unsigned int>(write_line.size())};
            ofs.write(reinterpret_cast<const char *>(&bin_header), sizeof(BinHeader));
            ofs.write(reinterpret_cast<const char *>(write_line.data()), sizeof(float) * write_line.size());
        }

        // load data from bin file
        {
            BinReader bin_reader{bin_file_name};
            read_line = bin_reader.data();
        }

        // confirm that read write is the same
        REQUIRE(write_line.size() == read_line.size());
        for (size_t i{}; i < write_line.size(); ++i) {
            CHECK(write_line[i] == doctest::Approx{read_line[i]});
        }
    }

    TEST_CASE("read moon's elevation from binary file and draw") {
        const BinReader bin_reader{get_binary_file_name()};

        auto create_input = [&bin_reader]() -> std::vector<Complex> {
            std::vector<Complex> input;
            input.reserve(bin_reader.data().size());
            for (size_t i{}; i < bin_reader.data().size(); ++i) {
                input.emplace_back(bin_reader.data()[i], 0);
            }
            return input;
        };

        // create input data from bin file
        const std::vector<Complex> input = create_input();
        REQUIRE(input.size() != 0);

        for (auto &cutoff_threshold : {0.01f, 0.025f, 0.05f, 0.1f, 1.0f}) {
            draw_graphs(input, cutoff_threshold, "moons", false, false);
        }
    }
}
